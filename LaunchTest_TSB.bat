REM # Script to start the automation - invoke TestComplete tool and pass the Main Driver

@echo off

"C:\Program Files (x86)\SmartBear\TestComplete 9\Bin\TestComplete.exe" "C:\SagePayment_TestAutomation\SagePayment_WSTK_TSB\SagePayment_WSTK_TSB.pjs" /r /project:WebChannelTSB /exit 


IF ERRORLEVEL 3 GOTO CannotRun
IF ERRORLEVEL 2 GOTO Errors
IF ERRORLEVEL 1 GOTO Warnings
IF ERRORLEVEL 0 GOTO Success

:CannotRun
ECHO The script cannot be run
GOTO End

:Errors
ECHO There are errors
GOTO End

:Warnings
ECHO There are warnings
GOTO End

:Success
ECHO No errors
GOTO End

:End